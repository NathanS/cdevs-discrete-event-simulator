#include "../tracers/TracerVerbose.h"
#include "../AtomicDevs.h"
#include "../../examples/atomicmodel_test/model.h"
#include "../exceptions/LogicDevsException.h"
#include <iostream>
#include "gtest/gtest.h"
#include "../Controller.h"
#include "atomicmodel_test/model.h"

namespace cdevs {

class TracerVerboseTest: public ::testing::Test
{
protected:
	void Constructors()
	{
		std::shared_ptr<TracerVerbose> t1 = std::make_shared<TracerVerbose>(controller_, "out.txt");
		EXPECT_EQ(t1->filename_, "out.txt");
		EXPECT_EQ(t1->prevtime_, cdevs::DevsTime(-1, -1));
		EXPECT_FALSE(t1->stream_);

		std::shared_ptr<TracerVerbose> t2 = std::make_shared<TracerVerbose>(controller_);
		EXPECT_EQ(t2->filename_, "");
		EXPECT_EQ(t2->prevtime_, cdevs::DevsTime(-1, -1));
		EXPECT_FALSE(t2->stream_);

		std::shared_ptr<TracerVerbose> t3 = std::make_shared<TracerVerbose>();
		EXPECT_EQ(t3->filename_, "");
		EXPECT_EQ(t3->prevtime_, cdevs::DevsTime(-1, -1));
		EXPECT_FALSE(t3->stream_);
	}

	void StartStop()
	{
		std::shared_ptr<TracerVerbose> t = std::make_shared<TracerVerbose>(controller_,  "out.txt");
		EXPECT_FALSE(t->stream_);
		EXPECT_THROW(t->PrintString("Error"), LogicDevsException);
		t->StartTracer(false);
		EXPECT_TRUE(t->stream_);
		t->StopTracer();
		EXPECT_FALSE(t->stream_);
		std::ifstream o("out.txt");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("", out.str());
		remove("out.txt");

		t = std::make_shared<TracerVerbose>(controller_, "");
		//Redirect cout
		std::stringbuf* my_cout = new std::stringbuf();
		std::streambuf *old = std::cout.rdbuf(my_cout);

		EXPECT_FALSE(t->stream_);
		t->StartTracer(false);
		EXPECT_EQ(&std::cout, t->stream_);
		t->StopTracer();
		//Restore cout
		std::cout.rdbuf(old);
		EXPECT_EQ("", my_cout->str());
		EXPECT_FALSE(t->stream_);
		delete my_cout;
	}

	void Print()
	{
		std::shared_ptr<TracerVerbose> t = std::make_shared<TracerVerbose>(controller_, "out.txt");
		t->StartTracer(false);
		t->PrintString("This is a test");
		t->StopTracer();
		std::ifstream o("out.txt");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("This is a test", out.str());
		EXPECT_THROW(t->PrintString("Error!"), LogicDevsException);
		remove("out.txt");

		t = std::make_shared<TracerVerbose>(controller_,  "");
		//Redirect cout
		std::stringbuf* my_cout = new std::stringbuf();
		std::streambuf *old = std::cout.rdbuf(my_cout);
		t->StartTracer(false);
		t->PrintString("This is a test");
		t->StopTracer();
		//Restore cout
		std::cout.rdbuf(old);
		EXPECT_EQ("This is a test", my_cout->str());
		EXPECT_THROW(t->PrintString("Error!"), LogicDevsException);
	}

	void TraceInitialize()
	{
		std::shared_ptr<TracerVerbose> t = std::make_shared<TracerVerbose>(controller_, "out.txt");
		std::string modelpath = "examples/libAtomicModelTest";
		std::shared_ptr<AtomicDevs> model = cdevs_examples::atomic::TrafficLight::create("VerkeersLicht");
		model->set_time_next(DevsTime(60, 0));
		t->StartTracer(false);
		t->TraceInitialize(model, DevsTime());
		controller_->PerformActions();
		t->StopTracer();
		std::ifstream o("out.txt");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/verbose/TraceInitialize.txt");
		std::stringstream exp;
		exp << e.rdbuf();
		EXPECT_EQ(exp.str(), out.str());
		remove("out.txt");
	}

	virtual void SetUp()
	{
		controller_ = Controller::create(NULL, 1);
	}
	virtual void TearDown()
	{

	}

	std::shared_ptr<Controller> controller_;
};

TEST_F(TracerVerboseTest, Constructors)
{
	Constructors();
}

TEST_F(TracerVerboseTest, StartStop)
{
	StartStop();
}

TEST_F(TracerVerboseTest, Print)
{
	Print();
}

TEST_F(TracerVerboseTest, TraceInitialize)
{
	TraceInitialize();
}

} /* namespace ns_DEVS */
