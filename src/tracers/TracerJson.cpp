#include "../tracers/TracerJson.h"

cdevs::TracerJson::TracerJson()

	: Tracer(), first_event_(true)

{
}

cdevs::TracerJson::TracerJson(std::shared_ptr<Controller> controller, std::string filename)

	: Tracer(controller, filename), first_event_(true)

{
}
cdevs::TracerJson::~TracerJson()
{
}

/**
 * \brief Starts the tracer
 *
 * @param recover  Flag determining whether the simulation is a recovered one
 */
void cdevs::TracerJson::StartTracer(bool recover)
{
	cdevs::Tracer::StartTracer(recover);
	if (!recover)
		PrintString("{\n\"trace\":{\n");
}

/**
 * \brief Stops the tracer
 */
void cdevs::TracerJson::StopTracer()
{
	PrintString("\n]\n}\n}");
	cdevs::Tracer::StopTracer();
}

/**
 * \brief Performs the initialize trace for the given
 * atomic model at the given time
 *
 * @param aDevs  The model to perform the initialize trace for
 * @param t  The time of calling the initialize trace
 */
void cdevs::TracerJson::TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t)
{
	std::string text = first_event_ ? "\"event\":[\n{" : ",\n{";
	text += "\n\"model\":\"" + aDEVS->get_model_full_name() + "\",\n";
	text += "\"time\":" + t.string() + ",\n";
	text += "\"kind\":\"EX\",\n";
	text += "\"state\":{" + aDEVS->getState().toJson() + "}\n}";
	if (first_event_)
		first_event_ = false;
	PrintString(text);
}

/**
 * \brief Performs the internal trace for the given
 * atomic model
 *
 * @param devs  The model to perform the internal trace for
 */
void cdevs::TracerJson::TraceInternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	Trace(aDEVS, aDEVS->get_time_last(), MakeTrace(aDEVS, true));
}

/**
 * \brief Performs the external trace for the given
 * atomic model
 *
 * @param aDevs  The model to perform the external trace for
 */
void cdevs::TracerJson::TraceExternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	Trace(aDEVS, aDEVS->get_time_last(), MakeTrace(aDEVS, false));

}

/**
 * \brief Performs the confluent trace for the given
 * atomic model
 *
 * @param aDevs  The model to perform the confluent trace for
 */
void cdevs::TracerJson::TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS)
{
	return Trace(aDEVS, aDEVS->get_time_last(), MakeTrace(aDEVS, false) + MakeTrace(aDEVS, true));
}

/**
 * \brief Performs a trace for the given
 * atomic model
 *
 * @param model  The model to perform the trace for
 * @param time  The time at which the trace is invoked
 * @param text  The text to output
 */
void cdevs::TracerJson::Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text)
{
	std::string text1 = ",\n";
	text1 += "{\n\"model\":\"" + model->get_full_name() + "\",\n";
	text1 += "\"time\":" + time.string() + ",\n";
	text1 += text;
	text1 += "}";
	PrintString(text1);
}

std::string cdevs::TracerJson::PortsToJson(std::string port_kind,
        std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
                std::owner_less<std::weak_ptr<Port> > >& my_output_)
{
	//unsigned nr_ports = my_output_.size();
	unsigned p_count = 0;
	std::string text = "";
	for (auto port_msgs_pair : my_output_) {
		text += "\"" + port_kind + " name\":\"" + port_msgs_pair.first.lock()->get_port_name() + "\",\n";
		text += "\"message\":[";
		for (std::list<std::shared_ptr<const EventBase> >::iterator it = port_msgs_pair.second.begin();
		        it != port_msgs_pair.second.end(); ++it) {
			if (!it->get())
				continue;
			if (it != port_msgs_pair.second.begin())
				text += ",\n";
			else
				text += "\n";
			text += "\"" + (*it)->string() + "\"\n";
		}
		text += "],\n";
		++p_count;
	}
	return text;
}

std::string cdevs::TracerJson::MakeTrace(std::shared_ptr<AtomicDevs> aDEVS, bool internal)
{
	std::string trans_kind;
	std::string ports;
	if (internal) {
		trans_kind = "IN";
		ports = PortsToJson("outport", aDEVS->my_output_);
	} else {
		trans_kind = "EX";
		ports = PortsToJson("inport", aDEVS->my_input_);
	}
	std::string text = "";
	text += "\"kind\":\"" + trans_kind + "\",\n";
	text += ports;
	text += "\"state\":{" + aDEVS->getState().toJson() + "}\n";
	return text;
}
