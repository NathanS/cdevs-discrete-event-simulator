#ifndef SRC_GVTCONTROLMESSAGE_H_
#define SRC_GVTCONTROLMESSAGE_H_

#include <map>

#include <cereal/access.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/map.hpp>

#include <cereal/access.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/map.hpp>

namespace cdevs
{
class GvtControlMessage
{
public:
	GvtControlMessage();
	GvtControlMessage(double clock, double send, std::map<unsigned, int> wait_vector, std::map<unsigned, int> accumulate_vector);

	const double get_clock() const;
	const double get_send() const;
	const std::map<unsigned, int>& get_wait_vector() const;
	const std::map<unsigned, int>& get_accumulate_vector() const;

	void set_clock(double clock);
	void set_send(double send);
	void set_wait_vector(std::map<unsigned, int> wait_vector);
	void set_accumulate_vector(std::map<unsigned, int> accumlate_vector);

private:
	friend class cereal::access;

	double clock_;
	double send_;
	std::map<unsigned, int> wait_vector_;
	std::map<unsigned, int> accumulate_vector_;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(clock_, send_, wait_vector_, accumulate_vector_);
	}
};
}


#endif /* SRC_GVTCONTROLMESSAGE_H_ */
