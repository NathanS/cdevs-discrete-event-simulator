/*
 * model.h
 *
 *  Created on: 7-mei-2015
 *      Author: david
 */

#ifndef EXAMPLES_ATOMICMODEL_TEST_MODEL_H_
#define EXAMPLES_ATOMICMODEL_TEST_MODEL_H_

#include "../../src/AtomicDevs.h"
#include "../../src/utility.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs_examples
{
namespace atomic
{
enum Modes
{
	kTrafficLightMode = 0,
	kPolicemanMode
};

class TrafficLightMode: public cdevs::State<TrafficLightMode>, public cdevs::EventCRTP<TrafficLightMode, kTrafficLightMode>
{
public:
	TrafficLightMode(std::string value = "red");
	std::string string() const;
	std::string toXML() const;

	std::string getValue() const;
	void setValue(std::string value);
private:
	std::string value_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<TrafficLightMode> >(this), value_);
	}
};

class TrafficLight: public cdevs::Atomic<TrafficLight, TrafficLightMode>
{
public:
	TrafficLight(std::string name="");

	TrafficLightMode const& ExtTransition(std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > inputs);

	TrafficLightMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> INTERRUPT;
	std::weak_ptr<cdevs::Port> OBSERVED;
private:
	TrafficLightMode state_red_ = TrafficLightMode("red");
	TrafficLightMode state_green_ = TrafficLightMode("green");
	TrafficLightMode state_yellow_ = TrafficLightMode("yellow");
	TrafficLightMode state_grey_ = TrafficLightMode("grey");
	TrafficLightMode state_manual_ = TrafficLightMode("manual");

//	TrafficLight() {};

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<TrafficLight, TrafficLightMode>>(this), INTERRUPT, OBSERVED, state_red_, state_green_, state_yellow_, state_manual_);
	}
};

inline std::string TrafficLightMode::getValue() const
{
	return value_;
}
inline void TrafficLightMode::setValue(std::string value)
{
	value_ = value;
}

inline std::string TrafficLightMode::string() const {
	return value_;
}

inline std::string TrafficLightMode::toXML() const {
	return "<color>"+value_+"</color>\n";
}

}
}
#endif /* EXAMPLES_ATOMICMODEL_TEST_MODEL_H_ */

#include <cereal/archives/binary.hpp>

//CEREAL_REGISTER_TYPE(cdevs_examples::atomic::TrafficLightMode)
//CEREAL_REGISTER_TYPE(cdevs_examples::atomic::TrafficLight)
