#=====================================================================#
# Project Setup                                                       #
#=====================================================================#
# Set minimal requirements on the CMake version to be used #
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.8)

#============================================================================
# Project name:
#============================================================================
project(CDevs)

#============================================================================
# Testing:
#============================================================================
enable_testing()
add_test(NAME test_all COMMAND "${PROJECT_SOURCE_DIR}/bin/test_all" WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})

#============================================================================
# Options for the compiler
#============================================================================
set(CMAKE_BUILD_TYPE "Release")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -w -std=c++11")
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -Wl,--no-as-needed -pg")
endif()
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -Wall -fmessage-length=0 -g -O0")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -w -fmessage-length=0 -O3")

#============================================================================
# Install sub-directory definitions
#============================================================================
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/bin")

#============================================================================
# Doxygen configuration
#============================================================================

FIND_PACKAGE(Doxygen)
IF (DOXYGEN_FOUND)
	SET(DOXYGEN_INPUT "${PROJECT_SOURCE_DIR}/doc/Doxyfile")
	SET(DOXYGEN_OUTPUT "${PROJECT_SOURCE_DIR}/doc")
	
	ADD_CUSTOM_COMMAND(
		OUTPUT ${DOXYGEN_OUTPUT}
		COMMAND ${CMAKE_COMMAND} -E echo_append "Building API Documentation..."
		COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_INPUT}
		COMMAND ${CMAKE_COMMAND} -E echo "Done."
		WORKING_DIRECTORY ${DOXYGEN_OUTPUT}
		DEPENDS ${DOXYGEN_INPUT}
		)
		
	add_custom_target (doc
                     COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_INPUT}
                     SOURCES ${PROJECT_BINARY_DIR}
                     WORKING_DIRECTORY ${DOXYGEN_OUTPUT})
ENDIF (DOXYGEN_FOUND)

#=====================================================================#
# Inlcude Linked Libraries                                            #
#=====================================================================#
# Include gtest #
include_directories(${PROJECT_SOURCE_DIR}/lib/gtest-1.7.0/include) 
include_directories(${PROJECT_SOURCE_DIR}/lib/gtest-1.7.0/)
include_directories(${PROJECT_SOURCE_DIR}/lib/cereal-1.1.0/include)

# add gtest libraries #
add_library(gtest ${PROJECT_SOURCE_DIR}/lib/gtest-1.7.0/src/gtest-all.cc) 
add_library(gtest_main ${PROJECT_SOURCE_DIR}/lib/gtest-1.7.0/src/gtest_main.cc)

#=====================================================================#
# Subdirectories                                                      #
#=====================================================================#

add_subdirectory(examples)
add_subdirectory(benchmarks)
add_subdirectory(src)
